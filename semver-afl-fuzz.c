#include "semver.h"
#include <stdio.h>
#include <string.h>

const char target_ver[] = "3.14.1-dev.5+926";

int
main(int argc, char** argv) {
  semver_t ver;
  semver_t target;
  char input[1024];
  memset(input, 0, sizeof(input));

  {
    int ret = fscanf(stdin, "%1023s", input);

    if (ret != 1) {
      return -1;
    }

    printf("parsing %s\n", input);
    int error = semver_parse(input, &ver);

    if (error != 0) {
      semver_free(&ver);
      return -2;
    }
  }


  {
    int error = semver_parse(target_ver, &target);

    if (error != 0) {
      semver_free(&ver);
      semver_free(&target);
      return -3;
    }
  }

  int resolution = semver_compare(target, ver);

  semver_free(&ver);
  semver_free(&target);
  return resolution;
}
