from conans import ConanFile, CMake, tools


class SemvercConan(ConanFile):
    name = "semver-c"
    version = "1.0.1-pre.1"
    license = "MIT"
    author = "David Yip <dwyip@peach-bun.com>"
    url = "https://gitlab.peach-bun.com/yipdw/semver.c"
    description = "Semantic version v2.0 parser and render written in ANSI C with zero dependencies"
    topics = ("semver")
    settings = "os", "compiler", "build_type", "arch"
    options = {
        "fPIC": [True, False],
        "lto": [True, False]
    }
    default_options = {
        "fPIC": False,
        "lto": False
    }
    generators = "cmake"
    exports_sources = "*"

    def source(self):
        tools.replace_in_file("CMakeLists.txt", "project(semver)",
                '''
                project(semver)
                include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
                conan_basic_setup()
                ''')

    def build(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_POSITION_INDEPENDENT_CODE"] = self.options.fPIC
        cmake.definitions["CMAKE_INTERPROCEDURAL_OPTIMIZATION"] = self.options.lto
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy("*.h", dst="include/semver")
        self.copy("*.a", dst="lib", src="lib")
        self.copy("*.lib", dst="lib", src="lib")

    def package_info(self):
        self.cpp_info.libs = ["semver"]

    def configure(self):
        del self.settings.compiler.libcxx
