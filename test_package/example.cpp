#include <iostream>
#include <semver/semver.h>

const char version[] = "3.14.15-pre.9+265pi";

int
main() {
	semver_t ver = {};
  int err = semver_parse(version, &ver);

  if (err != 0) {
    semver_free(&ver);
    return -1;
  }

  semver_free(&ver);
  return 0;
}
