import os

from conans import ConanFile, CMake, tools


class SemvercTestConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    options = {
        "lto": [True, False],
        "fPIC": [True, False]
    }

    def config_options(self):
        self.options.fPIC = self.options["semver-c"].fPIC
        self.options.lto = self.options["semver-c"].lto

    def build(self):
        cmake = CMake(self)
        # Current dir is "test_package/build/<build_id>" and CMakeLists.txt is
        # in "test_package"
        cmake.definitions["CMAKE_POSITION_INDEPENDENT_CODE"] = self.options.fPIC
        cmake.definitions["CMAKE_INTERPROCEDURAL_OPTIMIZATION"] = self.options.lto

        if self.options.lto:
            cmake.definitions["CMAKE_POLICY_DEFAULT_CMP0069"] = "NEW"

        cmake.configure()
        cmake.build()

    def imports(self):
        self.copy("*.dll", dst="bin", src="bin")
        self.copy("*.dylib*", dst="bin", src="lib")
        self.copy('*.so*', dst='bin', src='lib')

    def test(self):
        if not tools.cross_building(self.settings):
            os.chdir("bin")
            self.run(".%sexample" % os.sep)
